import { CategoryUpdateComponent } from './components/category-update/category-update.component';
import { BookUpdateComponent } from './components/book-update/book-update.component';
import { BookViewComponent } from './components/book-view/book-view.component';

import { BookFormComponent } from './components/book-form/book-form.component';
import { CategoryFormComponent } from './components/category-form/category-form.component';
import { CategoryComponent } from './components/category/category.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'book/add',
    component: BookFormComponent,
  },
  {
    path: 'book/view/:id',
    component: BookViewComponent,
  },
  {
    path: 'book/update/:id',
    component: BookUpdateComponent,
  },
  {
    path: 'category',
    component: CategoryComponent,
  },
  {
    path: 'category/add',
    component: CategoryFormComponent,
  },
  {
    path: 'category/update/:id',
    component: CategoryUpdateComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
export const routingComponents = [
  HomeComponent,
  CategoryComponent,
  CategoryFormComponent,
  CategoryUpdateComponent,
  BookFormComponent,
  BookViewComponent,
  BookUpdateComponent
];
