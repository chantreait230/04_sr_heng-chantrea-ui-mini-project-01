
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  BASE_URL = 'http://localhost:8080/api/v1/books';
  URL_POST_THUMBNAIL = 'http://localhost:8080/api/v1/thumbnail';

  constructor(private http: HttpClient, private router: Router) {}

  getAllBook() {
    return this.http.get<any[]>(this.BASE_URL);
  }

  getOneBook(id) {
    return this.http.get<any>(this.BASE_URL + `/${id}`);
  }

  postBook(object: Object) {
    return this.http.post(this.BASE_URL, object).subscribe((res) => {
      Swal.fire({
        text: 'Book has been inserted successfully!',
        icon: 'success',
        timer: 1500,
        showConfirmButton: false,
      }).then(() => {
        this.router.navigateByUrl('/');
      });
    });
  }

  updateBook(object: Object, id) {
    return this.http.put(this.BASE_URL + `/${id}`, object).subscribe((res) => {
      Swal.fire({
        text: 'Book has been updated successfully!',
        icon: 'success',
        timer: 1500,
        showConfirmButton: false,
      }).then(() => {
        this.router.navigateByUrl('/');
      });
    });
  }

  deleteBook(id) {
    return this.http.delete(this.BASE_URL + `/${id}`);
  }


  uploadFile = (myFile, cb) => {
    this.http.post(this.URL_POST_THUMBNAIL, myFile).subscribe((res: any) => {
      cb(res.data);
    });
  };
}
