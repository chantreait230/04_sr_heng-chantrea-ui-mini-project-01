import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  BASE_URL = 'http://localhost:8080/api/v1/categories';

  constructor(private http: HttpClient, private router: Router) {}

  getAllCategory() {
    return this.http.get<any[]>(this.BASE_URL);
  }

  getOneCategory(id) {
    return this.http.get<any>(this.BASE_URL+`/${id}`);
  }

  postCategory(object: Object) {
    return this.http.post(this.BASE_URL, object).subscribe((res) => {
      Swal.fire({
        text: 'Category has been inserted successfully!',
        icon: 'success',
        timer: 1500,
        showConfirmButton: false
      }).then(() => {
        this.router.navigateByUrl('/category');
      });
    });
  }

  updateCategory(object: Object, id) {
    return this.http.put(this.BASE_URL+`/${id}`, object).subscribe((res) => {
      Swal.fire({
        text: 'Category has been updated successfully!',
        icon: 'success',
        timer: 1500,
        showConfirmButton: false
      }).then(() => {
        this.router.navigateByUrl('/category');
      });
    });
  }

  deleteCategory(id){
    return this.http.delete(this.BASE_URL+`/${id}`);
  }


}
