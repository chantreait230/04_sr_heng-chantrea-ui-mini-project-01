import { CategoryService } from './../../services/category-service/category.service';

import { BookService } from './../../services/book-service/book.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  books = [];
  data = [];
  categories: [];
  title: any;
  page: number = 1;

  constructor(private _book: BookService, private _category: CategoryService) {}

  getBook() {
    this._book.getAllBook().subscribe((response: any) => {
      this.books = response._embedded.books;
    });
  }

  getCategory() {
    this._category.getAllCategory().subscribe((response: any) => {
      this.categories = response._embedded.categories;
    });
  }

  ngOnInit(): void {
    this.getCategory();
    this.getBook();
  }

  onSearch() {
    if (this.title == '') {
      this.ngOnInit();
    } else {
      this.books = this.books.filter((res) => {
        return res.title
          .toLocaleLowerCase()
          .match(this.title.toLocaleLowerCase());
      });
    }
  }

  onDeleteBook(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.value) {
        Swal.fire({
          title: 'Deleted',
          text: 'Category has been deleted successfully!',
          icon: 'success',
          timer: 1500,
        });
        this._book
          .deleteBook(id)
          .subscribe(
            () => (this.books = this.books.filter((item) => item.id !== id))
          );
      }
    });
  }
}
