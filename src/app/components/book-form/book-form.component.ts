import { BookService } from './../../services/book-service/book.service';
import { CategoryService } from './../../services/category-service/category.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css'],
})
export class BookFormComponent implements OnInit {
  categories: [];
  imageSrc: string =
    'https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png';
  book: object;
  file = '';
  isImage: boolean= false;

  bookForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private _category: CategoryService,
    private _book: BookService
  ) {
    this.bookForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      category: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  getCategory() {
    this._category.getAllCategory().subscribe((response: any) => {
      this.categories = response._embedded.categories;
    });
  }

  ngOnInit(): void {
    this.getCategory();
  }

  onSubmit() {

    let myFile = new FormData();
    myFile.append('file', this.file);

    if(this.isImage == false){
        this.book = {
          title: this.bookForm.get('title').value,
          author: this.bookForm.get('author').value,
          category:{
            id: this.bookForm.get('category').value
          },
          description: this.bookForm.get('description').value,
          thumbnail: this.imageSrc
        } 
        this._book.postBook(this.book);
    }else if(this.isImage == true){

      this._book.uploadFile(myFile, (cb) => {
      
        this.book = {
          title: this.bookForm.get('title').value,
          author: this.bookForm.get('author').value,
          category:{
            id: this.bookForm.get('category').value
          },
          description: this.bookForm.get('description').value,
          thumbnail: cb
        } 
        this._book.postBook(this.book);
      });
    }

    
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.file = event.target.files[0];
      this.isImage = true;

      reader.readAsDataURL(file);

      reader.onload = () => {
        this.imageSrc = reader.result as string;
        this.bookForm.patchValue({
          fileSource: reader.result,
        });
      };
    }
  }

  get title() {
    return this.bookForm.get('title');
  }

  get author() {
    return this.bookForm.get('author');
  }

  get description() {
    return this.bookForm.get('description');
  }

  get category() {
    return this.bookForm.get('category');
  }
}
