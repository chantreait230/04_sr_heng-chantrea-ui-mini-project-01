import { CategoryService } from './../../services/category-service/category.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css'],
})
export class CategoryFormComponent implements OnInit {
  registerForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private _category: CategoryService) {
    this.registerForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.maxLength(30)]],
    });
  }

  ngOnInit(): void {
    
  }

  onSubmit(){
    this._category.postCategory(this.registerForm.value);
  }

  get title() {
    return this.registerForm.get('title');
  }
}
