import { CategoryService } from './../../services/category-service/category.service';
import {
  Component,
  OnInit
} from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
})
export class CategoryComponent implements OnInit {
  data = [];

  constructor(private _category: CategoryService) {}

  getCategory() {
    this._category.getAllCategory().subscribe((response: any) => {
      this.data = response._embedded.categories;
    });
  }

  ngOnInit(): void {
    this.getCategory();
  }

  onDeleteCategory(id) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You will not be able to recover this imaginary file!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it',
    }).then((result) => {
      if (result.value) {
        Swal.fire({
          title: 'Deleted',
          text: 'Category has been deleted successfully!',
          icon: 'success',
          timer: 1500,
        });
        this._category.deleteCategory(id).subscribe(() =>
            (this.data = this.data.filter((item) =>
              item.id !== id
            ))
        );
      }
    });
  }
}
