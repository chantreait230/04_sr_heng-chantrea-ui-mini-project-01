import { CategoryService } from './../../services/category-service/category.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-category-update',
  templateUrl: './category-update.component.html',
  styleUrls: ['../category-form/category-form.component.css']
})
export class CategoryUpdateComponent implements OnInit {
  id: any;
  registerForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private _category: CategoryService, private _activatedRoute : ActivatedRoute) {
    this.registerForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.maxLength(30)]],
    });
    this.id=this._activatedRoute.snapshot.params.id;
  }

  onSubmit(){
    this._category.updateCategory(this.registerForm.value, this.id);
  }

  get title() {
    return this.registerForm.get('title');
  }

  ngOnInit(): void {
    this._category.getOneCategory(this.id).subscribe((response:any) => {
      this.registerForm.setValue({
        title: response.title
      })
    });
  }

}
