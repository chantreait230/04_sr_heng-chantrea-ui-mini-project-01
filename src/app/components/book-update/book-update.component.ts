import { BookService } from './../../services/book-service/book.service';
import { CategoryService } from './../../services/category-service/category.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AnimateChildOptions } from '@angular/animations';

@Component({
  selector: 'app-book-update',
  templateUrl: './book-update.component.html',
  styleUrls: ['../book-form/book-form.component.css']
})
export class BookUpdateComponent implements OnInit {

  categories: [];
  imageSrc: string =
    'https://lunawood.com/wp-content/uploads/2018/02/placeholder-image.png';
  book: object;
  file = '';
  isImage: boolean= false;
  id: number;

  bookForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private _category: CategoryService,
    private _book: BookService,
    private _activatedRoute : ActivatedRoute
  ) {
    this.bookForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      category: ['', Validators.required],
      description: ['', Validators.required]
    });
    this.id=this._activatedRoute.snapshot.params.id;
  }

  getCategory() {
    this._category.getAllCategory().subscribe((response: any) => {
      this.categories = response._embedded.categories;
    });
  }

  ngOnInit(): void {
    this.getCategory();
    this._book.getOneBook(this.id).subscribe((response)=>{
      this.bookForm.setValue({
        title: response.title,
        author: response.author,
        category: response.category.id,
        description: response.description
      })
      this.imageSrc = response.thumbnail;
    })
  }

  onSubmit() {

    let myFile = new FormData();
    myFile.append('file', this.file);

    if(this.isImage == false){
        this.book = {
          title: this.bookForm.get('title').value,
          author: this.bookForm.get('author').value,
          category:{
            id: this.bookForm.get('category').value
          },
          description: this.bookForm.get('description').value,
          thumbnail: this.imageSrc
        } 
        this._book.updateBook(this.book,this.id);
    }else if(this.isImage == true){

      this._book.uploadFile(myFile, (cb) => {
      
        this.book = {
          title: this.bookForm.get('title').value,
          author: this.bookForm.get('author').value,
          category:{
            id: this.bookForm.get('category').value
          },
          description: this.bookForm.get('description').value,
          thumbnail: cb
        } 
        this._book.updateBook(this.book, this.id);
      });
    }

    
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.file = event.target.files[0];
      this.isImage = true;

      reader.readAsDataURL(file);

      reader.onload = () => {
        this.imageSrc = reader.result as string;
        this.bookForm.patchValue({
          fileSource: reader.result,
        });
      };
    }
  }

  get title() {
    return this.bookForm.get('title');
  }

  get author() {
    return this.bookForm.get('author');
  }

  get description() {
    return this.bookForm.get('description');
  }

  get category() {
    return this.bookForm.get('category');
  }
}
