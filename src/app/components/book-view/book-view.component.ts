import { BookService } from './../../services/book-service/book.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.css']
})
export class BookViewComponent implements OnInit {

  id: number;
  book;

  constructor(private _activatedRoute : ActivatedRoute, private _book: BookService) {
    this.id=this._activatedRoute.snapshot.params.id;
   }

  ngOnInit(): void {
    this._book.getOneBook(this.id).subscribe((response)=>{
      this.book = response;
    })
  }

}
